<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\StoresUsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\StoresUsersTable Test Case
 */
class StoresUsersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\StoresUsersTable
     */
    public $StoresUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.stores_users',
        'app.users',
        'app.orders',
        'app.stores',
        'app.same_day_orders',
        'app.suppliers',
        'app.products',
        'app.products_stores',
        'app.products_suppliers',
        'app.stores_suppliers',
        'app.invoices',
        'app.product_stores'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('StoresUsers') ? [] : ['className' => 'App\Model\Table\StoresUsersTable'];
        $this->StoresUsers = TableRegistry::get('StoresUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->StoresUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
