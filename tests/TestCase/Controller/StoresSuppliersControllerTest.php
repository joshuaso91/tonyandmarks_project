<?php
namespace App\Test\TestCase\Controller;

use App\Controller\StoresSuppliersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\StoresSuppliersController Test Case
 */
class StoresSuppliersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.stores_suppliers',
        'app.stores',
        'app.orders',
        'app.same_day_orders',
        'app.products',
        'app.products_stores',
        'app.suppliers',
        'app.products_suppliers',
        'app.users',
        'app.stores_users'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
