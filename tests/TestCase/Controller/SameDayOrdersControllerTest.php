<?php
namespace App\Test\TestCase\Controller;

use App\Controller\SameDayOrdersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\SameDayOrdersController Test Case
 */
class SameDayOrdersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.same_day_orders',
        'app.stores',
        'app.orders',
        'app.users',
        'app.stores_users',
        'app.suppliers',
        'app.products',
        'app.products_stores',
        'app.products_suppliers',
        'app.stores_suppliers',
        'app.invoices'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
