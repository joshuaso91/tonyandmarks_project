<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Hash;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;

#use App\Model\ProductsSuppliers;
/**
 * ProductsSuppliers Controller
 *
 * @property \App\Model\Table\ProductsSuppliersTable $ProductsSuppliers
 */
class ProductsSuppliersController extends AppController
{

    /**
     * Default Cake Bake Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        // ProductsSuppliers::find('all')->toArray();
        $product_suppliers_table = TableRegistry::get('ProductsSuppliers');
        $product_suppliers = $product_suppliers_table->find('all')->distinct(['supplier_id'])->contain('Suppliers')->toArray();

        $this->set(compact('product_suppliers'));
        $this->set('_serialize', ['products_suppliers']);
        // $this->paginate = [
        //     'contain' => ['Suppliers', 'Products']
        // ];
        // $productsSuppliers = $this->paginate($this->ProductsSuppliers);

        // $this->set(compact('productsSuppliers'));
        // $this->set('_serialize', ['productsSuppliers']);
    }
    public function getSuppliersProducts()
    {
        $product_suppliers_table = TableRegistry::get('ProductsSuppliers');
        $supplier_id = $this->request->getQuery('supplier_id');
        $stores = TableRegistry::get('Stores')->find('all')->toArray();
        if (isset($supplier_id)) {
            $products = $product_suppliers_table->find('all')->where(['supplier_id'=>$supplier_id])->contain(['Products'])->toArray();
        } else {
            $prodcts = [];
        }
        $this->set(compact('products', 'stores'));
        $this->set('_serialize', ['products','stores']);
        $this->render('get_suppliers_products', 'ajax');
    }
    public function updateProduct()
    {
        $id = (int)$this->request->getData('id');
        $res = [];
        $res['error'] = 1;
        if ($id > 0) {
            $products_suppliers_table = TableRegistry::get('ProductsSuppliers');
            $product = $products_suppliers_table->get($id, [
                'contain' => ['Products']
            ]);
            $price = (float)$this->request->getData('bargain_price');
            if ($this->request->is(['patch', 'post', 'put']) && $this->request->is('Ajax')) {
                if (is_int($price) || is_float($price) && $price > 0) {
                    $product = $products_suppliers_table->patchEntity($product, $this->request->data);
                    if ($products_suppliers_table->save($product)) {
                        $res['error'] = 0;
                        $res['price'] = sprintf("$%01.2f/%s", $product->bargain_price, $product->product->unit);
//                        debug($product);die();
                    } else {
                        $res['error'] = 2;
                    }
                }
            }
        }
        $this->set('res', json_encode($res));
        $this->render('update_product', 'ajax');
    }
    public function __checkDuplicates($store_id, $product_id, $delete)
    {
        if ($delete) {
            if ($store_id == 0) {
                $stores = TableRegistry::get('stores')->find('all')->toArray();
                foreach ($stores as $store) {
                    $this->__checkDuplicates($store->id, $product_id, $delete);
                }
            } else {
                $ps = TableRegistry::get('ProductsStores');
                $query = $ps->query();
                $query->delete()->where([
                    'store_id'=>$store_id,
                    'product_id'=>$product_id
                ])->execute();
            }
        } else {
            if ($store_id == 0) {
                $stores = TableRegistry::get('stores')->find('all')->toArray();
                foreach ($stores as $store) {
                    $this->__checkDuplicates($store->id, $product_id, $delete);
                }
            } else {
                $res = TableRegistry::get('ProductsStores')->find('all')->where([
                    'store_id'=>$store_id,
                    'product_id'=>$product_id
                ])->toArray();
                if (count($res)==0) {
                    $products_stores_table = TableRegistry::get('ProductsStores');
                    $ps = $products_stores_table->newEntity();
                    $ps->store_id = $store_id;
                    $ps->product_id = $product_id;
                    $ps->total_req = 1;
                    $products_stores_table->save($ps);
                }
            }
        }
    }
    public function updateOrder()
    {
        $delete = false;
        if ($this->request->getData('action') != "add") {
            $delete = true;
        }
        $this->__checkDuplicates($this->request->getData('store_id'), $this->request->getData('id'), $delete);
        $this->render('update_product', 'ajax');
    }
    /**
     * View method
     *
     * @param string|null $id Products Supplier id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $productsSupplier = $this->ProductsSuppliers->get($id, [
            'contain' => ['Suppliers', 'Products']
        ]);

        $this->set('productsSupplier', $productsSupplier);
        $this->set('_serialize', ['productsSupplier']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $productIds = Hash::extract($this->request->getData(), 'product_id'); // extracts the product_ids array from the request
            $keys = ['supplier_id', 'product_id', 'date_entered', 'product_price', 'notes']; //this is just my array of keys of the column of the database

            $dataTosave = []; // this loops through the array of product _ids and generates a new value array, combines it with the keys array and pushes it to dataToSave
            foreach ($productIds as $id) {
                $vals = [
                    Hash::get($this->request->getData(), 'supplier_id'),
                    $id,
                    Hash::get($this->request->getData(), 'date_entered'),
                    Hash::get($this->request->getData(), 'product_price'),
                    Hash::get($this->request->getData(), 'notes'),
                ];

                array_push($dataTosave, array_combine($keys, $vals)); // the array_combine($keys, $vals) just combines the two arrays as a key => value array
            }

            $productsSuppliersTable =  TableRegistry::get('ProductsSuppliers');
               
            $productsSuppliers = $productsSuppliersTable->newEntities($dataTosave);

            $productsSuppliersTable->connection()->transactional(function () use ($productsSuppliersTable, $productsSuppliers) {
                foreach ($productsSuppliers as $productsSupplier) {
                    $productsSuppliersTable->save($productsSupplier, ['atomic' => false]);
                }
                $this->Flash->success(__('The stocktake has been saved.'));

                return $this->redirect(['action' => 'index']);
            });

            //$this->Flash->error(__('The stocktake could not be saved. Please, try again.'));
        }
        $productsSupplier = $this->ProductsSuppliers->newEntity();
    //    $storesUsers = $this->ProductsStores->StoresUsers->find('list')->contain(
    //                                                                 [
    //                                                                     'Users' => ['fields' => ['user_name']],
    //                                                                     'Stores' => ['fields' => ['store_name']]
    //                                                                 ]);
       $suppliers = $this->ProductsSuppliers->Suppliers->find('list');
        $products = $this->ProductsSuppliers->Products->find('list');
        $this->set(compact('productsSupplier', 'suppliers', 'products'));
        $this->set('_serialize', ['productsStore']);
    }

    /**
     * Default Cake Bake Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    // public function add()
    // {
    //     $productsSupplier = $this->ProductsSuppliers->newEntity();
    //     if ($this->request->is('post')) {
    //         $productsSupplier = $this->ProductsSuppliers->patchEntity($productsSupplier, $this->request->data);
    //         if ($this->ProductsSuppliers->save($productsSupplier)) {
    //             $this->Flash->success(__('The {0} has been saved.', 'Products Supplier'));
    //             return $this->redirect(['action' => 'index']);
    //         } else {
    //             $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Products Supplier'));
    //         }
    //     }
    //     $suppliers = $this->ProductsSuppliers->Suppliers->find('list', ['limit' => 200]);
    //     $products = $this->ProductsSuppliers->Products->find('list', ['limit' => 200]);
    //     $this->set(compact('productsSupplier', 'suppliers', 'products'));
    //     $this->set('_serialize', ['productsSupplier']);
    // }

    /**
        ***********************************************************************************************
        ***********************************************************************************************
        ***********************************************************************************************
        **** start of updateProductSupplierPrice() ****
    **/
    public function updateProductSupplierPrice()
    {
        $value = $this->request->data['value'];
        $supplier_id = $this->request->data['supplier_id'];
        $product_id = $this->request->data['product_id'];
        $column_name = $this->request->data['column_name'];

        $this->request->data['product_price'] = $value;

        $options = array(
                'conditions'=>array(
                    'AND'=>array(
                        'supplier_id'=>$supplier_id,
                        'product_id'=>$product_id
                    )
                )
            );
        $productsSuppliers = $this->ProductsSuppliers->find('all', $options)->first();
        $productsSupplier = $this->ProductsSuppliers->patchEntity($productsSuppliers, $this->request->data);
        if ($this->ProductsSuppliers->save($productsSupplier)) {
            $response = array('status'=>true,'message'=>'Update was successful');
        } else {
            $response = array('status'=>false,'message'=>'Update was not successful');
        }
        $this->autoRender = false; // Set Render False
        $this->response->body(json_encode($response));
        return $this->response;
    }
    /**
        ***********************************************************************************************
        ***********************************************************************************************
        ***********************************************************************************************
            **** END OF updateProductsSuppliersPrice() ****
    **/

    /**
     * Edit method
     *
     * @param string|null $id Products Supplier id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $productsSupplier = $this->ProductsSuppliers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $productsSupplier = $this->ProductsSuppliers->patchEntity($productsSupplier, $this->request->data);
            if ($this->ProductsSuppliers->save($productsSupplier)) {
                $this->Flash->success(__('The {0} has been saved.', 'Products Supplier'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Products Supplier'));
            }
        }
        $suppliers = $this->ProductsSuppliers->Suppliers->find('list', ['limit' => 200]);
        $products = $this->ProductsSuppliers->Products->find('list', ['limit' => 200]);
        $this->set(compact('productsSupplier', 'suppliers', 'products'));
        $this->set('_serialize', ['productsSupplier']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Products Supplier id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $productsSupplier = $this->ProductsSuppliers->get($id);
        if ($this->ProductsSuppliers->delete($productsSupplier)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Products Supplier'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Products Supplier'));
        }
        return $this->redirect(['action' => 'index']);
    }


    public function supplierProducts($supplier_id)
    {
        $this->viewBuilder()->layout('');
        $productsSuppliers = $this->ProductsSuppliers->find('all')->contain(['Products'])->where(['supplier_id'=>$supplier_id]);

        //debug($productsSuppliers);
        $this->set(compact('productsSuppliers'));
        $this->set(compact('supplier_id'));
        $this->set('_serialize', ['productsSuppliers']);
    }
}
