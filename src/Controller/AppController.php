<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;

use Cake\Core\Configure;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
                'flash' => [
                    'element' => 'error',
                    'key' => 'auth'
                ],
                'authenticate' => [
                    'Form' => [
                        'fields' => [
                            'username' => 'email',
                            'password' => 'password'
                        ]
                    ]
                ],
                'loginAction' =>
                [
                    'controller' => 'Users',
                    'action' => 'login'
                ],
                'storage' => 'Session'
            ]); // enables Authentication component for users to login in order to view any content of the system.
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }

        //adding code as per "maiconpinto/cakephp-adminlte-theme"
        $this->viewBuilder()->theme('AdminLTE');


        $this->set('theme', Configure::read('Theme'));

        //Checking whether users is logged in or not.
        if ($this->request->session()->read('Auth.User')) {
            $this->set('loggedIn', true);
        } else {
            $this->set('loggedIn', false);
        }


        // *********************************************************************************** //

        //*** This is to allow who views what for the views or .ctp files only.  ***//

        //Check if it's the Super Administrator
        // This method is a new way to write set sessions and check database at the same time compared to CakePHP 2.x
        // Previously we need to create a function method and do many steps, but they revamped it in a way that you just have to write this
        // and a cleaner and faster way of coding.
        if ($this->request->session()->read('Auth.User.roles') == 'Super Admin') {
            $this->set('superAdmin', true);
        } else {
            $this->set('superAdmin', false);
        }

        // Check if it's the Admin

        if ($this->request->session()->read('Auth.User.roles') == 'Admin') {
            $this->set('admin', true);
        } else {
            $this->set('admin', false);
        }

        // Check if it's the Staff
        if ($this->request->session()->read('Auth.User.roles') == 'Moderator') {
            $this->set('moderator', true);
        } else {
            $this->set('moderator', false);
        }

        //Getting User username who logged into the database
        // This code here reads the database of from the Users table and set a session to who loggedIn.
        $loggedInUsername = $this->request->session()->read('Auth.User.username');
        if ($loggedInUsername) {
            $this->set('users_username', $loggedInUsername);
        }
        $loggedInUserRoles = $this->request->session()->read('Auth.User.role');
        if ($loggedInUserRoles) {
            $this->set('users_roles', $loggedInUserRoles);
        }

        // *********************************************************************************** //
        // *********************************************************************************** //
    }

    public function beforeFilter(Event $event)
    {
        $this->viewBuilder()->layout('custom_default');

        return parent::beforeFilter($event); // TODO: Change the autogenerated stub
    }
}
