<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 */
class OrdersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $results = [];
        $productStoresTable = TableRegistry::get('ProductsStores');
        $productSuppliersTable = TableRegistry::get('ProductsSuppliers');
        $suppliersTable = TableRegistry::get('Suppliers');
        $storesTable = TableRegistry::get('Stores');
        $stores = $storesTable->find('all')->toArray();
        $productsIds = [];
        $productStores = $productStoresTable->find('all')->distinct(['product_id'])->contain(['Products'])->toArray();
        foreach ($productStores as $productStore) {
            $results[$productStore->product->name] = [];
            $results[$productStore->product->name]["product_id"] = $productStore->product_id;
            $count = 0;
            foreach ($stores as $store) {
                $storeProducts = $productStoresTable->find()->where(['store_id'=>$store->id,'product_id'=>$productStore->product->id])->first();
                $qty = isset($storeProducts->total_req)?$storeProducts->total_req:0;
                $count += $qty;
                $results[$productStore->product->name][$store->name] = $qty;
            }
            if(!$count){
                unset($results[$productStore->product->name]);
            }
        }
        foreach ($results as $result){
            $productsIds[] += $result['product_id'];
        }
        $suppliers = $productSuppliersTable->find()->distinct(['supplier_id'])->contain(['Suppliers'])->where(function ($exp, $q) use ($productsIds) {
            return $exp->in('product_id', $productsIds);
        })->toArray();
        $this->set(compact('results', 'stores','suppliers'));
        $this->set('_serialize', ['results','stores','suppliers']);
    }
    public function showSupplierOrder(){
        $results = [];
        $productStoresTable = TableRegistry::get('ProductsStores');
        $storesTable = TableRegistry::get('Stores');
        $stores = $storesTable->find('all')->toArray();
        $supplier_id = $this->request->getQuery('supplier_id');
        $productSuppliersTable = TableRegistry::get('ProductsSuppliers');
        $productStores = $productStoresTable->find('all')->distinct(['product_id'])->contain(['Products'])->toArray();
        $productsIds = [];
        foreach ($productStores as $productStore) {
            $results[$productStore->product->name] = [];
            $results[$productStore->product->name]["product_id"] = $productStore->product_id;
            $count = 0;
            foreach ($stores as $store) {
                $storeProducts = $productStoresTable->find()->where(['store_id'=>$store->id,'product_id'=>$productStore->product->id])->first();
                $qty = isset($storeProducts->total_req)?$storeProducts->total_req:0;
                $count += $qty;
                $results[$productStore->product->name][$store->name] = $qty;
            }
            if(!$count){
                unset($results[$productStore->product->name]);
            }
        }
        foreach ($results as $result){
            $productsIds[] += $result['product_id'];
        }
        $suppliers = $productSuppliersTable->find('all')->where(function ($exp, $q) use ($productsIds) {
            return $exp->in('product_id', $productsIds);
        })->where(['supplier_id'=>$supplier_id])->toArray();
        $productsIds = [];
        foreach ($suppliers as $supplier){
            $productsIds[] += $supplier['product_id'];
        }

        ###filter
        $filters = [];
        $productStores = $productStoresTable->find('all')->distinct(['product_id'])->contain(['Products'])->toArray();
        foreach ($productStores as $productStore) {
            $filters[$productStore->product->name] = [];
            $filters[$productStore->product->name]["product_id"] = $productStore->product_id;
            if (!in_array($productStore->product_id,$productsIds)){
                unset($filters[$productStore->product->name]);
                continue;
            }
            $count = 0;
            foreach ($stores as $store) {
                $storeProducts = $productStoresTable->find()->where(['store_id'=>$store->id,'product_id'=>$productStore->product->id])->first();
                $qty = isset($storeProducts->total_req)?$storeProducts->total_req:0;
                $count += $qty;
                $filters[$productStore->product->name][$store->name] = $qty;
            }
            if(!$count){
                unset($filters[$productStore->product->name]);
            }
        }
        $this->set(compact('filters','stores'));
        $this->set('_serialize', ['filters','stores']);
        $this->render('show_supplier_order','ajax');
    }
    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => ['Stores', 'Users', 'Suppliers', 'Products', 'Invoices']
        ]);

        $this->set('order', $order);
        $this->set('_serialize', ['order']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $order = $this->Orders->newEntity();
        if ($this->request->is('post')) {
            $order = $this->Orders->patchEntity($order, $this->request->data);
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The {0} has been saved.', 'Order'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Order'));
            }
        }
        $stores = $this->Orders->Stores->find('list', ['limit' => 200]);
        $users = $this->Orders->Users->find('list', ['limit' => 200]);
        $suppliers = $this->Orders->Suppliers->find('list', ['limit' => 200]);
        $products = $this->Orders->Products->find('list', ['limit' => 200]);
        $this->set(compact('order', 'stores', 'users', 'suppliers', 'products'));
        $this->set('_serialize', ['order']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $order = $this->Orders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $order = $this->Orders->patchEntity($order, $this->request->data);
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('The {0} has been saved.', 'Order'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Order'));
            }
        }
        $stores = $this->Orders->Stores->find('list', ['limit' => 200]);
        $users = $this->Orders->Users->find('list', ['limit' => 200]);
        $suppliers = $this->Orders->Suppliers->find('list', ['limit' => 200]);
        $products = $this->Orders->Products->find('list', ['limit' => 200]);
        $this->set(compact('order', 'stores', 'users', 'suppliers', 'products'));
        $this->set('_serialize', ['order']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($id);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Order'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Order'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
