<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Hash;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;

/**
 * StoresSuppliers Controller
 *
 * @property \App\Model\Table\StoresSuppliersTable $StoresSuppliers
 */
class StoresSuppliersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->loadModel('Suppliers');
        $this->loadModel('Stores');
        $user = $this->request->session()->read('Auth.User');
        $user_id = $user['id'];
        //debug($user);

        //finding the Stores that's matching with the User_id logged in.
        $stores = $this->Stores->find('all')->select('id')->matching('Users', function ($q) use ($user_id) {
            return $q->where(['Users.id' => $user_id]);
        })->toArray(); //putting the data to an array
        
        //making a new collection of the data found in array and extracting the id of it to an array
        $store_id = (new Collection($stores))->extract('id')->filter()->toArray();
        //debug($store_id);

        //Getting the StoresSuppliers list
        $storesSuppliers = $this->StoresSuppliers->find('all')->matching('Stores', function ($q) use ($store_id) {
            return $q->where(['Stores.id IN' => $store_id]);
        });
        //debug($query->toArray());

        $this->paginate = [
            'contain' => ['Stores', 'Suppliers']
        ];
        $storesSuppliers = $this->paginate($storesSuppliers);
        
        $this->set(compact('storesSuppliers'));
        $this->set('_serialize', ['storesSuppliers']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    // public function index()
    // {
    //     $this->paginate = [
    //         'contain' => ['Stores', 'Suppliers']
    //     ];
    //     $storesSuppliers = $this->paginate($this->StoresSuppliers);

    //     $this->set(compact('storesSuppliers'));
    //     $this->set('_serialize', ['storesSuppliers']);
    // }

    /**
     * View method
     *
     * @param string|null $id Stores Supplier id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $storesSupplier = $this->StoresSuppliers->get($id, [
            'contain' => ['Stores', 'Suppliers']
        ]);

        $this->set('storesSupplier', $storesSupplier);
        $this->set('_serialize', ['storesSupplier']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $supplierIds = Hash::extract($this->request->getData(), 'supplier_id'); // extracts the supplier_ids array from the request
            $keys = ['store_id', 'supplier_id']; //this is just my array of keys of the column of the database

            $dataTosave = []; // this loops through the array of supplier_ids and generates a new value array, combines it with the keys array and pushes it to dataToSave
            foreach ($supplierIds as $id) {
                $vals = [
                    Hash::get($this->request->getData(), 'store_id'),
                    $id,
                ];

                array_push($dataTosave, array_combine($keys, $vals)); // the array_combine($keys, $vals) just combines the two arrays as a key => value array
            }

            $storesSuppliersTable =  TableRegistry::get('StoresSuppliers');
               
            $storesSuppliers = $storesSuppliersTable->newEntities($dataTosave);

            $storesSuppliersTable->connection()->transactional(function () use ($storesSuppliersTable, $storesSuppliers) {
                foreach ($storesSuppliers as $storesSupplier) {
                    $storesSuppliersTable->save($storesSupplier, ['atomic' => false]);
                }
                $this->Flash->success(__('The Store Suppliers has been saved.'));

                return $this->redirect(['action' => 'index']);
            });

            //$this->Flash->error(__('The stocktake could not be saved. Please, try again.'));
        }
        $storesSupplier = $this->StoresSuppliers->newEntity();
        $stores = $this->StoresSuppliers->Stores->find('list');
        $suppliers = $this->StoresSuppliers->Suppliers->find('list');
        $this->set(compact('storesSupplier', 'stores', 'suppliers'));
        $this->set('_serialize', ['storesSupplier']);
    }
    /**
     * Default Cake Bake Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    // public function add()
    // {
    //     $storesSupplier = $this->StoresSuppliers->newEntity();
    //     if ($this->request->is('post')) {
    //         $storesSupplier = $this->StoresSuppliers->patchEntity($storesSupplier, $this->request->data);
    //         if ($this->StoresSuppliers->save($storesSupplier)) {
    //             $this->Flash->success(__('The {0} has been saved.', 'Stores Supplier'));
    //             return $this->redirect(['action' => 'index']);
    //         } else {
    //             $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Stores Supplier'));
    //         }
    //     }
    //     $stores = $this->StoresSuppliers->Stores->find('list', ['limit' => 200]);
    //     $suppliers = $this->StoresSuppliers->Suppliers->find('list', ['limit' => 200]);
    //     $this->set(compact('storesSupplier', 'stores', 'suppliers'));
    //     $this->set('_serialize', ['storesSupplier']);
    // }
    

    /**
     * Edit method
     *
     * @param string|null $id Stores Supplier id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $storesSupplier = $this->StoresSuppliers->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $storesSupplier = $this->StoresSuppliers->patchEntity($storesSupplier, $this->request->data);
            if ($this->StoresSuppliers->save($storesSupplier)) {
                $this->Flash->success(__('The {0} has been saved.', 'Stores Supplier'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Stores Supplier'));
            }
        }
        $stores = $this->StoresSuppliers->Stores->find('list', ['limit' => 200]);
        $suppliers = $this->StoresSuppliers->Suppliers->find('list', ['limit' => 200]);
        $this->set(compact('storesSupplier', 'stores', 'suppliers'));
        $this->set('_serialize', ['storesSupplier']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Stores Supplier id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $storesSupplier = $this->StoresSuppliers->get($id);
        if ($this->StoresSuppliers->delete($storesSupplier)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Stores Supplier'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Stores Supplier'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
