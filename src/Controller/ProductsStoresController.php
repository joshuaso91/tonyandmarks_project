<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Hash;
use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;

/**
 * ProductsStores Controller
 *
 * @property \App\Model\Table\ProductsStoresTable $ProductsStores
 */
class ProductsStoresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        //        $this->loadModel('Products');
//        $this->loadModel('Stores');
//        $user = $this->request->session()->read('Auth.User');
//        $user_id = $user['id'];
//        //debug($user);
//
//        //finding the Stores that's matching with the User_id logged in.
//        $stores = $this->Stores->find('all')->select('id')->matching('Users', function ($q) use ($user_id) {
//        return $q->where(['Users.id' => $user_id]);
//        })->toArray(); //putting the data to an array
//
//        //making a new collection of the data found in array and extracting the id of it to an array
//        $store_id = (new Collection($stores))->extract('id')->filter()->toArray();
//        //debug($store_id);
//
//        //Getting the ProductsStores list
//        $productsStores = $this->ProductsStores->find('all')->matching('Stores', function ($q) use ($store_id) {
//            return $q->where(['Stores.id IN' => $store_id]);
//        });
//        //debug($query->toArray());
//
//        $this->paginate = [
//            'contain' => ['Stores', 'Products']
//        ];
//        $productsStores = $this->paginate($productsStores);
//
//        $this->set(compact('productsStores'));
//        $this->set('_serialize', ['productsStores']);
//        $storages_table = TableRegistry::get('Storages');
//        $stores_table = TableRegistry::get('Stores');
//        $stores = $stores_table->find('all')->contain([])->toArray();
        $store_table = TableRegistry::get('Stores');
        $stores = $store_table->find('all')->toArray();
        $this->set(compact('stores'));
        $this->set('_serialize', ['stores']);
    }
    public function showStorage()
    {
        // debug();
//        $storages_table = TableRegistry::get('Storages');
//        $storages = $storages_table->find('all')->where([
//            'store_id'=>$this->request->query['store_id']
//        ])->contain(['Products'])->toArray();
        $storages = $this->ProductsStores->find('all')->where([
            'store_id'=>$this->request->getQuery('store_id')
        ])->contain(['Products'])->toArray();
        $store = TableRegistry::get('Stores')->get($this->request->getQuery('store_id'), ['contain'=>[]]);
        $this->set(compact('storages', 'store'));
        $this->set('_serialize', ['storages','store']);
        $this->render('show_storage', 'ajax');
    }
    public function updateStorage()
    {
        $storage = $this->ProductsStores->get($this->request->getData('id'));
        $res = [];
        $res['error'] = 1;
        $refill = (int)$this->request->getData('total_req');
        if ($this->request->is(['patch', 'post', 'put'])) {
            if (is_int($refill) && $refill >= 0) {
                $storage = $this->ProductsStores->patchEntity($storage, $this->request->data);
                if ($this->ProductsStores->save($storage)) {
                    $res['error'] = 0;
                    $res['total_req'] = $refill;
                } else {
                    $res['error'] = 2;
                }
            }
        }
        $this->set('res', json_encode($res));
        $this->render('update_storage', 'ajax');
    }
    /**
     * View method
     *
     * @param string|null $id Products Store id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $productsStore = $this->ProductsStores->get($id, [
            'contain' => ['Stores', 'Products']
        ]);

        $this->set('productsStore', $productsStore);
        $this->set('_serialize', ['productsStore']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        if ($this->request->is('post')) {
            $productIds = Hash::extract($this->request->getData(), 'product_id'); // extracts the product_ids array from the request
            $keys = ['store_id', 'product_id', 'current_stock', 'suggested_order', 'total_req']; //this is just my array of keys of the column of the database

            $dataTosave = []; // this loops through the array of product _ids and generates a new value array, combines it with the keys array and pushes it to dataToSave
            foreach ($productIds as $id) {
                $vals = [
                    Hash::get($this->request->getData(), 'store_id'),
                    $id,
                    Hash::get($this->request->getData(), 'current_stock'),
                    Hash::get($this->request->getData(), 'suggested_order'),
                    Hash::get($this->request->getData(), 'total_req'),
                ];

                array_push($dataTosave, array_combine($keys, $vals)); // the array_combine($keys, $vals) just combines the two arrays as a key => value array
            }

            $productsStoresTable =  TableRegistry::get('ProductsStores');
               
            $productsStores = $productsStoresTable->newEntities($dataTosave);

            $productsStoresTable->connection()->transactional(function () use ($productsStoresTable, $productsStores) {
                foreach ($productsStores as $productsStore) {
                    $productsStoresTable->save($productsStore, ['atomic' => false]);
                }
                $this->Flash->success(__('The stocktake has been saved.'));

                return $this->redirect(['action' => 'index']);
            });

            //$this->Flash->error(__('The stocktake could not be saved. Please, try again.'));
        }
        $productsStore = $this->ProductsStores->newEntity();
    //    $storesUsers = $this->ProductsStores->StoresUsers->find('list')->contain(
    //                                                                 [
    //                                                                     'Users' => ['fields' => ['user_name']],
    //                                                                     'Stores' => ['fields' => ['store_name']]
    //                                                                 ]);
        $stores = $this->ProductsStores->Stores->find('list');
        $products = $this->ProductsStores->Products->find('list');
        $this->set(compact('productsStore', 'stores', 'products'));
        $this->set('_serialize', ['productsStore']);
    }

    /**
     * Default Cake Bake Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    // public function add()
    // {
    //     $productsStore = $this->ProductsStores->newEntity();
    //     if ($this->request->is('post')) {
    //         $productsStore = $this->ProductsStores->patchEntity($productsStore, $this->request->data);
    //         if ($this->ProductsStores->save($productsStore)) {
    //             $this->Flash->success(__('The {0} has been saved.', 'Products Store'));
    //             return $this->redirect(['action' => 'index']);
    //         } else {
    //             $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Products Store'));
    //         }
    //     }
    //     $stores = $this->ProductsStores->Stores->find('list');
    //     $products = $this->ProductsStores->Products->find('list');
    //     $this->set(compact('productsStore', 'stores', 'products'));
    //     $this->set('_serialize', ['productsStore']);
    // }


    /**
        ***********************************************************************************************
        ***********************************************************************************************
        ***********************************************************************************************
            **** This three save functions() is for ProductsStore index auto save after loosing focus ****
    **/


    public function updateProductStores()
    {
        $value = $this->request->data['value'];
        $store_id = $this->request->data['store_id'];
        $product_id = $this->request->data['product_id'];
        $column_name = $this->request->data['column_name'];

        // if($column_name == "current_stock"){
        //      $this->request->data['current_stock'] = $value;
        // }else if($column_name == "suggested_order"){
        //      $this->request->data['suggested_order'] = $value;
        // }else

        if ($column_name == "total_req") {
            $this->request->data['total_req'] = $value;
        }
        
        $options = array(
                'conditions'=>array(
                    'AND'=>array(
                        'store_id'=>$store_id,
                        'product_id'=>$product_id
                    )
                )
            );
        $productsStores = $this->ProductsStores->find('all', $options)->first();
        $productsStore = $this->ProductsStores->patchEntity($productsStores, $this->request->data);
        if ($this->ProductsStores->save($productsStore)) {
            $response = array('status'=>true,'message'=>'Update was successful');
        } else {
            $response = array('status'=>false,'message'=>'Update was not successful');
        }
        $this->autoRender = false; // Set Render False
        $this->response->body(json_encode($response));
        return $this->response;
    }

    // *** initial way of adding a column to the database.

    // public function saveCurrentStock(){

    //     $value = $this->request->data['value'];
    //     $store_id = $this->request->data['store_id'];
    //     $product_id = $this->request->data['product_id'];
    //     $options = array(
    //             'conditions'=>array(
    //                 'AND'=>array(
    //                     'store_id'=>$store_id,
    //                     'product_id'=>$product_id
    //                 )
    //             )
    //         );
    //     $productsStores = $this->ProductsStores->find('all',$options)->first();
    //     //$productsStore = $this->ProductsStores->find('all')->where(['store_id'=>$store_id,'$product_id'=>$product_id])->first();
        
    //     $this->request->data['current_stock'] = $value;

    //     //$productsStore->current_stock = $value;
    //     $productsStore = $this->ProductsStores->patchEntity($productsStores, $this->request->data);
    //     if($this->ProductsStores->save($productsStore)) {
    //         $response = array('status'=>true,'message'=>'Update was successful');
    //     }
    //     else {
    //         $response = array('status'=>false,'message'=>'Update was not successful');
    //     }
    //     $this->autoRender = false; // Set Render False
    //     $this->response->body(json_encode($response));
    //     return $this->response;

    ////////////////////////////////////////////////////////////////////////////////////////
            /** this function here is the test function before including it properly into the other function.*/
               /** Ignore this if possible. Just another way of writing it **/
        // if($this->request->is('Ajax')) //Ajax Detection
        // {
        //     $value = $this->request->data['value'];
        //     $store_id = $this->request->data['store_id'];
        //     $product_id = $this->request->data['product_id'];

        //     $options = array(
        //         'conditions'=>array(
        //             'AND'=>array(
        //                 'store_id'=>$store_id,
        //                 'product_id'=>$product_id
        //             )
        //         )
        //     );
        //     $productsStores = $this->ProductsStores->find('all',$options)->first();


        //     $this->request->data['current_stock'] = $value;
        
        //     $productsStore = $this->ProductsStores->patchEntity($productsStores, $this->request->data);
        //      if($this->ProductsStores->save($productsStore)) {
        //         $response = array('status'=>true,'message'=>'Update was successful');
        //     }
        //     else {
        //         $response = array("status"=>false,"message"=>"Update failed");
        //     }
        //     $this->autoRender = false; // Set Render False
        //     $this->response->body(json_encode($response));
        //     return $this->response;
        // }
  ///////////////////////////////////////////////////////////////////////////////////////////////////
        
    // }
/*
    public function saveSuggestedOrder() {
    //     $value = $this->request->data['value'];
    //     $store_id = $this->request->data['store_id'];
    //     $product_id = $this->request->data['product_id'];
    //     $options = array(
    //             'conditions'=>array(
    //                 'AND'=>array(
    //                     'store_id'=>$store_id,
    //                     'product_id'=>$product_id
    //                 )
    //             )
    //         );
    //     $productsStores = $this->ProductsStores->find('all',$options)->first();
    //     //$productsStore = $this->ProductsStores->find('all')->where(['store_id'=>$store_id,'$product_id'=>$product_id])->first();

    //     $this->request->data['suggested_order'] = $value;

    //     //$productsStore->suggested_order = $value;
    //     $productsStore = $this->ProductsStores->patchEntity($productsStores, $this->request->data);
    //     if($this->ProductsStores->save($productsStore)) {
    //         $response = array('status'=>true,'message'=>'Update was successful');
    //     }
    //     else {
    //         $response = array('status'=>false,'message'=>'Update was not successful');
    //     }
    //     $this->autoRender = false; // Set Render False
    //     $this->response->body(json_encode($response));
    //     return $this->response;
    }

    public function saveTotalReq() {
    //     $value = $this->request->data['value'];
    //     $store_id = $this->request->data['store_id'];
    //     $product_id = $this->request->data['product_id'];
    //     $options = array(
    //             'conditions'=>array(
    //                 'AND'=>array(
    //                     'store_id'=>$store_id,
    //                     'product_id'=>$product_id
    //                 )
    //             )
    //         );
    //     $productsStores = $this->ProductsStores->find('all',$options)->first();
    //     //$productsStore = $this->ProductsStores->find('all')->where(['store_id'=>$store_id,'$product_id'=>$product_id])->first();

    //     $this->request->data['total_req'] = $value;

    //     //$productsStore->total_req = $value;
    //     $productsStore = $this->ProductsStores->patchEntity($productsStores, $this->request->data);
    //     if($this->ProductsStores->save($productsStore)) {
    //         $response = array('status'=>true,'message'=>'Update was successful');
    //     }
    //     else {
    //         $response = array('status'=>false,'message'=>'Update was not successful');
    //     }
    //     $this->autoRender = false; // Set Render False
    //     $this->response->body(json_encode($response));
    //     return $this->response;
    } */

    /**
        ***********************************************************************************************
        ***********************************************************************************************
        ***********************************************************************************************

    **/

    /**
     * Edit method
     *
     * @param string|null $id Products Store id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $productsStore = $this->ProductsStores->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $productsStore = $this->ProductsStores->patchEntity($productsStore, $this->request->data);
            if ($this->ProductsStores->save($productsStore)) {
                $this->Flash->success(__('The {0} has been saved.', 'Products Store'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Products Store'));
            }
        }
        $stores = $this->ProductsStores->Stores->find('list');
        $products = $this->ProductsStores->Products->find('list');
        $this->set(compact('productsStore', 'stores', 'products'));
        $this->set('_serialize', ['productsStore']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Products Store id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $productsStore = $this->ProductsStores->get($id);
        if ($this->ProductsStores->delete($productsStore)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Products Store'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Products Store'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
