<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Stores Controller
 *
 * @property \App\Model\Table\StoresTable $Stores
 */
class StoresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $user =   $this->request->session()->read('Auth.User');
        $user_id = $user['id'];
        //debug($user);

        $stores = $this->Stores->find()->matching('Users', function ($q) use ($user_id) {
            return $q->where(['Users.id' => $user_id]);
        });

        $stores = $this->paginate($stores);

        $this->set(compact('stores'));
        $this->set('_serialize', ['stores']);
    }

    /**
     * View method
     *
     * @param string|null $id Store id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $store = $this->Stores->get($id, [
            'contain' => ['Products', 'Suppliers', 'Users', 'Orders', 'SameDayOrders']
        ]);

        $this->set('store', $store);
        $this->set('_serialize', ['store']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $store = $this->Stores->newEntity();
        if ($this->request->is('post')) {
            $store = $this->Stores->patchEntity($store, $this->request->data);
            if ($this->Stores->save($store)) {
                $this->Flash->success(__('The {0} has been saved.', 'Store'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Store'));
            }
        }
        $products = $this->Stores->Products->find('list', ['limit' => 200]);
        $suppliers = $this->Stores->Suppliers->find('list', ['limit' => 200]);
        $users = $this->Stores->Users->find('list', ['limit' => 200]);
        $this->set(compact('store', 'products', 'suppliers', 'users'));
        $this->set('_serialize', ['store']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Store id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $store = $this->Stores->get($id, [
            'contain' => ['Products', 'Suppliers', 'Users']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $store = $this->Stores->patchEntity($store, $this->request->data);
            if ($this->Stores->save($store)) {
                $this->Flash->success(__('The {0} has been saved.', 'Store'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Store'));
            }
        }
        $products = $this->Stores->Products->find('list', ['limit' => 200]);
        $suppliers = $this->Stores->Suppliers->find('list', ['limit' => 200]);
        $users = $this->Stores->Users->find('list', ['limit' => 200]);
        $this->set(compact('store', 'products', 'suppliers', 'users'));
        $this->set('_serialize', ['store']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Store id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $store = $this->Stores->get($id);
        if ($this->Stores->delete($store)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Store'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Store'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
