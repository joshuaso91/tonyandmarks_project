<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Suppliers Controller
 *
 * @property \App\Model\Table\SuppliersTable $Suppliers
 */
class SuppliersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $suppliers = $this->paginate($this->Suppliers);

        $this->set(compact('suppliers'));
        $this->set('_serialize', ['suppliers']);
    }

    /**
     * View method
     *
     * @param string|null $id Supplier id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $supplier = $this->Suppliers->get($id, [
            'contain' => ['Products', 'Stores', 'SameDayOrders']
        ]);

        $this->set('supplier', $supplier);
        $this->set('_serialize', ['supplier']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $supplier = $this->Suppliers->newEntity();
        if ($this->request->is('post')) {
            $supplier = $this->Suppliers->patchEntity($supplier, $this->request->data);
            if ($this->Suppliers->save($supplier)) {
                $this->Flash->success(__('The {0} has been saved.', 'Supplier'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Supplier'));
            }
        }
        $products = $this->Suppliers->Products->find('list', ['limit' => 200]);
        $stores = $this->Suppliers->Stores->find('list', ['limit' => 200]);
        $this->set(compact('supplier', 'products', 'stores'));
        $this->set('_serialize', ['supplier']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Supplier id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $supplier = $this->Suppliers->get($id, [
            'contain' => ['Products', 'Stores']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $supplier = $this->Suppliers->patchEntity($supplier, $this->request->data);
            if ($this->Suppliers->save($supplier)) {
                $this->Flash->success(__('The {0} has been saved.', 'Supplier'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The {0} could not be saved. Please, try again.', 'Supplier'));
            }
        }
        $products = $this->Suppliers->Products->find('list', ['limit' => 200]);
        $stores = $this->Suppliers->Stores->find('list', ['limit' => 200]);
        $this->set(compact('supplier', 'products', 'stores'));
        $this->set('_serialize', ['supplier']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Supplier id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $supplier = $this->Suppliers->get($id);
        if ($this->Suppliers->delete($supplier)) {
            $this->Flash->success(__('The {0} has been deleted.', 'Supplier'));
        } else {
            $this->Flash->error(__('The {0} could not be deleted. Please, try again.', 'Supplier'));
        }
        return $this->redirect(['action' => 'index']);
    }

    public function supplierList()
    {
        $this->viewBuilder()->layout('');
        $suppliers = $this->Suppliers->find('all');

      //  debug($suppliers);
        $this->set(compact('suppliers'));
        $this->set('_serialize', ['suppliers']);
    }

    public function supplierHome()
    {
    }

    public function supplierProducts($supplier_id)
    {
        $this->viewBuilder()->layout('');
        $products = $this->Suppliers->Products->find('all')->matching('Suppliers', function ($q) use ($supplier_id) {
            return $q->where(['Suppliers.id' => $supplier_id]);
        });
        $this->set(compact('products'));
        $this->set(compact('supplier_id'));
        $this->set('_serialize', ['products']);
    }
}
