<style>
    td{
        cursor:pointer;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Stores
        <div class="pull-right"><?= $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs']) ?></div>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="box-tools">
                        <form action="<?php echo $this->Url->build(); ?>" method="POST">
                            <div class="input-group input-group-sm"  style="width: 180px;">
                                <input type="text" name="search" class="form-control" placeholder="<?= __('Fill in to start search') ?>">
                                <span class="input-group-btn">
                <button class="btn btn-info btn-flat" type="submit"><?= __('Filter') ?></button>
                </span>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <section class="content">
                        <div class="row">
                            <div class="col-xs-2 col-lg-2 col-md-2">
                                <div id="stores">
                                    <table id ="storesListTable" class="table table-hover">
                                        <?php foreach ($stores as $store): ?>
                                            <tr>
                                                <td store-id="<?php echo $store->id;?>" onclick="loadProducts(this);"><?= h($store->name) ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </table>
                                </div>
                            </div>
                            <div class="col-xs-10 col-lg-10 col-md-10">
                                <div id="storages">
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<!-- /.content -->
<script>
    function loadProducts(store){
        store_id = $(store).attr('store-id');
        console.log(store_id);
        var urlPath = "<?php echo \Cake\Routing\Router::Url(array('controller'=>'ProductsStores','action'=>'showStorage'))?>";
        console.log(urlPath);
        // console.log();
        $.ajax({
            type:'GET',
            async: true,
            cache: false,
            url:urlPath+'?store_id='+store_id,
        }).done(function(data){
            // console.log(data);
            $("#storages").html(data);
        });

    }
</script>
<?php $this->end(); ?>