<?php //debug($storages) ?>
<h3 class="box-title"><?= h($store->name) ?></h3>
<table class="table table-hover">
    <tr>
        <th>Name</th>
        <th>Current Request</th>
        <th>Customized Request</th>
    </tr>
    <?php foreach ($storages as $storage): ?>
        <tr>
            <td style="width:25%"><?= h($storage->product->name) ?> </td>
            <td style="width:10%" data-product-store-id="<?= $storage->id ?>" class="total_req"><?= h($storage->total_req) ?> </td>
            <td data-change-price=true data-product-store-id="<?= $storage->id ?>">
                <form method="POST" class="form-inline price new">
                    <div class="form-group">
                        <input data-product-store-id="<?= $storage->id ?>" name="stock" type="text" class="form-control"
                               placeholder="Quantity Request">
                        <button class="btn btn-primary btn-update-req" data-product-store-id="<?= $storage->id ?>">Confirm</button>
                    </div>
                    </div>
                </form>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
<script>
    $(document).ready(function(){
        function updateStorage(storage_id,qty){
            if(qty=="")
                return false;
            var urlPath = "<?php echo \Cake\Routing\Router::Url(array('controller'=>'ProductsStores','action'=>'updateStorage'))?>";
            $.ajax({
                type:'POST',
                data:{
                    'id':storage_id,
                    'total_req':qty
                },
                async: true,
                cache: false,
                url:urlPath,
            }).done(function(data){
                data = JSON.parse(data);
                $("td.total_req[data-product-store-id="+storage_id+"]").html(data.total_req);
            });
        }
        $("button.btn-update-req").on('click',function(e){
            e.preventDefault();
            id = $(this).attr('data-product-store-id');
            qty = $("input[data-product-store-id="+id+"]").val()
            updateStorage(id,qty);
        });
        $("input[name='stock']")
            .keypress(function(e) {
                if(e.which == 13) {
                    var storage_id = $(this).attr('data-product-store-id');
                    qty = $(this).val();
                    updateStorage(storage_id,qty);
                    return e.which !== 13;
                }
            })
            .on('blur',function(){
                var storage_id = $(this).attr('data-product-store-id');
                qty = $(this).val();
                updateStorage(storage_id,qty);
            });
    });
</script>