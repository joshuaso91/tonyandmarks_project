<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Store Order
    <div class="pull-right"><?= $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs']) ?></div>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><?= __('List of') ?> Products in Stores</h3>
          <div class="box-tools">
            <form action="<?php echo $this->Url->build(); ?>" method="POST">
              <div class="input-group input-group-sm"  style="width: 180px;">
                <input type="text" name="search" class="form-control" placeholder="<?= __('Fill in to start search') ?>">
                <span class="input-group-btn">
                <button class="btn btn-info btn-flat" type="submit"><?= __('Filter') ?></button>
                </span>
              </div>
            </form>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <tr>
              <th><?= $this->Paginator->sort('id') ?></th>
              <th><?= $this->Paginator->sort('store_id') ?></th>
              <th><?= $this->Paginator->sort('product_id') ?></th>
              <th><?= $this->Paginator->sort('current_stock') ?></th>
              <th><?= $this->Paginator->sort('suggested_order') ?></th>
              <th><?= $this->Paginator->sort('total_req') ?></th>
              <th><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($productsStores as $productsStore): ?>
              <tr>
                <td><?= $this->Number->format($productsStore->id) ?></td>
                <td><?= $productsStore->has('store') ? $this->Html->link($productsStore->store->name, ['controller' => 'Stores', 'action' => 'view', $productsStore->store->id]) : '' ?></td>
                <td><?= $productsStore->has('product') ? $this->Html->link($productsStore->product->name, ['controller' => 'Products', 'action' => 'view', $productsStore->product->id]) : '' ?></td>
                <td><?= $this->Form->input('current_stock', ['label'=> null, 'value'=>$productsStore->current_stock,'id'=>$productsStore->product->id."c_s", 'onblur'=>'updateProductStore(this)','data-column-name'=>'current_stock', 'data-store-id'=>$productsStore->store->id, 'data-product-id'=>$productsStore->product->id]) ?></td>
                <td><?= $this->Form->input('suggested_order', ['label'=> null, 'value'=>$productsStore->suggested_order,'id'=>$productsStore->product->id."s_o", 'onblur'=>'updateProductStore(this)','data-column-name'=>'suggested_order', 'data-store-id'=>$productsStore->store->id, 'data-product-id'=>$productsStore->product->id]) ?></td>
                <td><?= $this->Form->input('total_req', ['label'=> null, 'value'=>$productsStore->total_req,'id'=>$productsStore->product->id."t_r",'data-column-name'=>'total_req', 'data-store-id'=>$productsStore->store->id, 'data-product-id'=>$productsStore->product->id]) ?></td>
                <td class="actions" style="white-space:nowrap">
                  <?= $this->Html->link(__('View'), ['action' => 'view', $productsStore->id], ['class'=>'btn btn-info btn-xs']) ?>
                  <?= $this->Html->link(__('Edit'), ['action' => 'edit', $productsStore->id], ['class'=>'btn btn-warning btn-xs']) ?>
                  <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $productsStore->id], ['confirm' => __('Confirm to delete this entry?'), 'class'=>'btn btn-danger btn-xs']) ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          <ul class="pagination pagination-sm no-margin pull-right">
            <?php echo $this->Paginator->numbers(); ?>
          </ul>
        </div>
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->

<?php $this->start('scriptBotton'); ?>
<script>
    $(document).ready(function () {

            $("#supplier").load('/suppliers/supplierList');

        $('#supplierListTable tr td').click(function(){
            //var cid = $(this).attr('id');
            //alert(cid);
            var cval = $(this).text();
            alert(cval);
        });

    });

    function move(td) {
        console.log(td.id);
    }

    function loadProducts(td)
    {
        $("#products").load('/ProductsSuppliers/supplierProducts/'+td.id);
    }

    function updateProductStore(textInput) {
      
      //alert ("value" +textInput.value );
      console.log("value entered => " +textInput.value );
      value = textInput.value;
      store_id=$(textInput).data('store-id');
      product_id=$(textInput).data('product-id');
      column_name=$(textInput).data('column-name');

      console.log('store-id: ' + store_id);
      console.log('product-id:' + product_id);   
      console.log('column-name: ' + column_name);

      var current_stock = $("#"+product_id+"c_s").val();
      var suggested_order = $("#"+product_id+"s_o").val();
      
      var urlPath = "<?php echo \Cake\Routing\Router::Url(array('controller'=>'products-stores','action'=>'updateProductStores'))?>";
      //alert(urlPath);
      $.ajax({
          type:'POST',
          data:{
            'value':value,
            'store_id':store_id,
            'product_id':product_id,
            'column_name':column_name,
            'suggested_order':suggested_order,
            'current_stock':current_stock
          },
          async: true,
          cache: false,
          url:urlPath,
          success: function(response) {
            
              console.log(response);
              var data = JSON.parse(response);
              var total_request = data.total_request;
              $("#"+product_id+"t_r").val(total_request);
          },
          
          error: function(xhr,textStatus,error){

            console.log(error);
                  
          }
        
      });
      
    
    }
</script>
<?php $this->end(); ?>


<!--// this function here is when there are including the suggested_order - current_stock to get the total request value
        FOR CONTROLLER CLASS
    
    // public function updateProductStores(){
    //     $value = $this->request->data['value'];
    //     $store_id = $this->request->data['store_id'];
    //     $product_id = $this->request->data['product_id'];
    //     $column_name = $this->request->data['column_name'];

    //     $current_stock = $this->request->data['current_stock'];
    //     $suggested_order = $this->request->data['suggested_order'];
    //     $total_request =  $current_stock - $suggested_order;
    //     $this->request->data['total_req'] = $total_request;

    //     if($column_name == "current_stock"){
    //          $this->request->data['current_stock'] = $value;
    //     }else if($column_name == "suggested_order"){
    //          $this->request->data['suggested_order'] = $value;
    //     }
        
    //     $options = array(
    //             'conditions'=>array(
    //                 'AND'=>array(
    //                     'store_id'=>$store_id,
    //                     'product_id'=>$product_id
    //                 )
    //             )
    //         );
    //     $productsStores = $this->ProductsStores->find('all',$options)->first();
    //     $productsStore = $this->ProductsStores->patchEntity($productsStores, $this->request->data);
    //     if($this->ProductsStores->save($productsStore)) {
    //         $response = array('status'=>true,'message'=>'Update was successful','total_request'=>$total_request);
    //     } 
    //     else {
    //         $response = array('status'=>false,'message'=>'Update was not successful');
    //     }
    //     $this->autoRender = false; // Set Render False
    //     $this->response->body(json_encode($response));
    //     return $this->response;
    // }-->