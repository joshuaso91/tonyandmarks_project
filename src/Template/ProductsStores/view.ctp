<section class="content-header">
  <h1>
    <?php echo __('Products Store'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false])?>
    </li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-info"></i>
                <h3 class="box-title"><?php echo __('Information'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                    <dt><?= __('Store') ?></dt>
                    <dd>
                        <?= $productsStore->has('store') ? $productsStore->store->name : '' ?>
                    </dd>
                    <dt><?= __('Product') ?></dt>
                    <dd>
                        <?= $productsStore->has('product') ? $productsStore->product->name : '' ?>
                    </dd>
                    <dt><?= __('Current Stock') ?></dt>
                    <dd>
                        <?= $this->Number->format($productsStore->current_stock) ?>
                    </dd>
                    <dt><?= __('Suggested Order') ?></dt>
                    <dd>
                        <?= $this->Number->format($productsStore->suggested_order) ?>
                    </dd>
                    <dt><?= __('Total Req') ?></dt>
                    <dd>
                        <?= $this->Number->format($productsStore->total_req) ?>
                    </dd>
                </dl>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

</section>
