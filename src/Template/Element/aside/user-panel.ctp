<?php
$file = $theme['folder'] . DS . 'src' . DS . 'Template' . DS . 'Element' . DS . 'aside' . DS . 'user-panel.ctp';

if (!file_exists($file)) {
    ob_start();
    include_once $file;
    echo ob_get_clean();
} else {
?>
<div class="user-panel">

    <div class="pull-left image">
        <?php echo $this->Html->image('paul-160x160.jpg', array('class' => 'img-circle', 'alt' => 'User Image')); ?>
    </div>
    <div class="pull-left info">
        <?php if(!$loggedIn): ?>
            <p>Currently Not Logged-in</p>
            <a href="#"><i class="fa fa-circle text-danger"></i> Offline</a>
        <?php else: ?>
            <p><?= $users_username; ?>:&nbsp;<?= $users_roles; ?></p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        <?php endif; ?>
    </div>
</div>
<?php } ?>