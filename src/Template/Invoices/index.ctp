<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Invoices
    <div class="pull-right"><?= $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs']) ?></div>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><?= __('List of') ?> Invoices</h3>
          <div class="box-tools">
            <form action="<?php echo $this->Url->build(); ?>" method="POST">
              <div class="input-group input-group-sm"  style="width: 180px;">
                <input type="text" name="search" class="form-control" placeholder="<?= __('Fill in to start search') ?>">
                <span class="input-group-btn">
                <button class="btn btn-info btn-flat" type="submit"><?= __('Filter') ?></button>
                </span>
              </div>
            </form>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
          <table class="table table-hover">
            <tr>
              <th><?= $this->Paginator->sort('id') ?></th>
              <th><?= $this->Paginator->sort('invoice_no') ?></th>
              <th><?= $this->Paginator->sort('order_id') ?></th>
              <th><?= $this->Paginator->sort('same_day_order_id') ?></th>
              <th><?= $this->Paginator->sort('invoice_date') ?></th>
              <th><?= $this->Paginator->sort('total') ?></th>
              <th><?= $this->Paginator->sort('payment') ?></th>
              <th><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($invoices as $invoice): ?>
              <tr>
                <td><?= $this->Number->format($invoice->id) ?></td>
                <td><?= h($invoice->invoice_no) ?></td>
                <td><?= $invoice->has('order') ? $this->Html->link($invoice->order->id, ['controller' => 'Orders', 'action' => 'view', $invoice->order->id]) : '' ?></td>
                <td><?= $invoice->has('same_day_order') ? $this->Html->link($invoice->same_day_order->id, ['controller' => 'SameDayOrders', 'action' => 'view', $invoice->same_day_order->id]) : '' ?></td>
                <td><?= h($invoice->invoice_date) ?></td>
                <td><?= $this->Number->format($invoice->total) ?></td>
                <td><?= $this->Number->format($invoice->payment) ?></td>
                <td class="actions" style="white-space:nowrap">
                  <?= $this->Html->link(__('View'), ['action' => 'view', $invoice->id], ['class'=>'btn btn-info btn-xs']) ?>
                  <?= $this->Html->link(__('Edit'), ['action' => 'edit', $invoice->id], ['class'=>'btn btn-warning btn-xs']) ?>
                  <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $invoice->id], ['confirm' => __('Confirm to delete this entry?'), 'class'=>'btn btn-danger btn-xs']) ?>
                </td>
              </tr>
            <?php endforeach; ?>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          <ul class="pagination pagination-sm no-margin pull-right">
            <?php echo $this->Paginator->numbers(); ?>
          </ul>
        </div>
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->
