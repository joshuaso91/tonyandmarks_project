<section class="content-header">
  <h1>
    <?php echo __('Invoice'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false])?>
    </li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-info"></i>
                <h3 class="box-title"><?php echo __('Information'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                                                                                                                <dt><?= __('Invoice No') ?></dt>
                                        <dd>
                                            <?= h($invoice->invoice_no) ?>
                                        </dd>
                                                                                                                                                    <dt><?= __('Order') ?></dt>
                                <dd>
                                    <?= $invoice->has('order') ? $invoice->order->id : '' ?>
                                </dd>
                                                                                                                <dt><?= __('Same Day Order') ?></dt>
                                <dd>
                                    <?= $invoice->has('same_day_order') ? $invoice->same_day_order->id : '' ?>
                                </dd>
                                                                                                
                                            
                                                                                                                                                            <dt><?= __('Total') ?></dt>
                                <dd>
                                    <?= $this->Number->format($invoice->total) ?>
                                </dd>
                                                                                                                <dt><?= __('Payment') ?></dt>
                                <dd>
                                    <?= $this->Number->format($invoice->payment) ?>
                                </dd>
                                                                                                
                                                                                                        <dt><?= __('Invoice Date') ?></dt>
                                <dd>
                                    <?= h($invoice->invoice_date) ?>
                                </dd>
                                                                                                                                                                                                            
                                            
                                                                        <dt><?= __('Invoice Notes') ?></dt>
                            <dd>
                            <?= $this->Text->autoParagraph(h($invoice->invoice_notes)); ?>
                            </dd>
                                                            </dl>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

</section>
