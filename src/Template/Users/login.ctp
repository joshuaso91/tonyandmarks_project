<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo isset($theme['title']) ? $theme['title'] : 'AdminLTE 2 | Log in'; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <?php echo $this->Html->css('AdminLTE./bootstrap/css/bootstrap'); ?>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <?php echo $this->Html->css('AdminLTE.AdminLTE.min'); ?>
  <!-- iCheck -->
  <?php echo $this->Html->css('AdminLTE./plugins/iCheck/square/blue'); ?>
  <!-- Tony & Marks Custom CSS -->
    <?php echo $this->Html->css('AdminLTE.tonymarks_custom'); ?>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo $this->Url->build(array('controller' => 'pages', 'action' => 'display', 'home')); ?>"><?php echo $theme['logo']['large'] ?></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <?php /* <p class="login-box-msg"><?php echo __('Sign in to start your session') ?></p> */ ?>
    <p> <?php echo $this->Flash->render(); ?> </p>
    <p> <?php echo $this->Flash->render('auth'); ?> </p>

			<div id="loginContainer_header" class="">
					<?php echo $this->Html->image('tonymarks_logo.png', array('class' => 'img-rectangle', 'alt' => 'User Image')); ?>
			</div>
			
			<div id="loginContainer_body" class="container-fluid">
			<?php echo $this->Flash->render('message') ?>
				<?= $this->Form->create(); ?>
					<?= $this->Form->input('email'); ?>
					<?= $this->Form->input('password', array('type' => 'password')); ?>
					<?= $this->Form->submit('Login'); ?>
				<?= $this->Form->end(); ?>
			</div>

    <?php
    if (isset($theme['login']['show_social']) && $theme['login']['show_social']) {
        ?>
        <div class="social-auth-links text-center">
          <p>- <?php echo __('OR') ?> -</p>
          <!--<a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> <?php echo __('Sign in using Facebook') ?></a>
          <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> <?php echo __('Sign in using Google+') ?></a>-->
        </div>
        <?php
    }
    ?>
    <div class="center-align">
      <?php
      /*if (isset($theme['login']['show_remember']) && $theme['login']['show_remember']) {
          ?>
          <a href="#"><?php echo __('I forgot my password') ?></a><br>
          <?php
      }*/
      if (isset($theme['login']['show_register']) && $theme['login']['show_register']) {
          ?>
          <!--<a href="#" class="text-center"><?php echo __('Register a new membership') ?></a>-->
          <?= $this->Html->link(__('Register a new User'), ['controller'=>'Users','action' => 'register'], ['class'=>'btn btn-primary btn-s']) ?>
          <?php
      }
      ?>
    </div

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.1.4 -->
<?php echo $this->Html->script('/plugins/jQuery/jQuery-2.1.4.min'); ?>
<!-- Bootstrap 3.3.5 -->
<?php echo $this->Html->script('/bootstrap/js/bootstrap'); ?>
<!-- iCheck -->
<?php echo $this->Html->script('/plugins/iCheck/icheck.min'); ?>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
