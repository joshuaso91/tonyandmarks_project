<section class="content-header">
  <h1>
    <?php echo __('Order'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false])?>
    </li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-info"></i>
                <h3 class="box-title"><?php echo __('Information'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                                                                                                        <dt><?= __('Store') ?></dt>
                                <dd>
                                    <?= $order->has('store') ? $order->store->name : '' ?>
                                </dd>
                                                                                                                <dt><?= __('User') ?></dt>
                                <dd>
                                    <?= $order->has('user') ? $order->user->username : '' ?>
                                </dd>
                                                                                                                <dt><?= __('Supplier') ?></dt>
                                <dd>
                                    <?= $order->has('supplier') ? $order->supplier->name : '' ?>
                                </dd>
                                                                                                                <dt><?= __('Product') ?></dt>
                                <dd>
                                    <?= $order->has('product') ? $order->product->name : '' ?>
                                </dd>
                                                                                                                        <dt><?= __('Notes') ?></dt>
                                        <dd>
                                            <?= h($order->notes) ?>
                                        </dd>
                                                                                                                                    
                                            
                                                                                                                                                            <dt><?= __('Ordered Qty') ?></dt>
                                <dd>
                                    <?= $this->Number->format($order->ordered_qty) ?>
                                </dd>
                                                                                                                <dt><?= __('Price') ?></dt>
                                <dd>
                                    <?= $this->Number->format($order->price) ?>
                                </dd>
                                                                                                                <dt><?= __('Total Price') ?></dt>
                                <dd>
                                    <?= $this->Number->format($order->total_price) ?>
                                </dd>
                                                                                                
                                                                                                        <dt><?= __('Ordered Date') ?></dt>
                                <dd>
                                    <?= h($order->ordered_date) ?>
                                </dd>
                                                                                                                                                                                                            
                                            
                                    </dl>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Invoices']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($order->invoices)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Invoice No
                                    </th>
                                        
                                                                    
                                    <th>
                                    Order Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Same Day Order Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Invoice Date
                                    </th>
                                        
                                                                    
                                    <th>
                                    Total
                                    </th>
                                        
                                                                    
                                    <th>
                                    Invoice Notes
                                    </th>
                                        
                                                                    
                                    <th>
                                    Payment
                                    </th>
                                        
                                                                                                                                            
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($order->invoices as $invoices): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($invoices->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($invoices->invoice_no) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($invoices->order_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($invoices->same_day_order_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($invoices->invoice_date) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($invoices->total) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($invoices->invoice_notes) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($invoices->payment) ?>
                                    </td>
                                                                                                            
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Invoices', 'action' => 'view', $invoices->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Invoices', 'action' => 'edit', $invoices->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Invoices', 'action' => 'delete', $invoices->id], ['confirm' => __('Are you sure you want to delete # {0}?', $invoices->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
