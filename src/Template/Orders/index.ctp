<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Purchases
        <div class="pull-right"><?= $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs']) ?></div>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="box-tools">
                        <form action="<?php echo $this->Url->build(); ?>" method="POST">
                            <div class="input-group input-group-sm"  style="width: 180px;">
                                <input type="text" name="search" class="form-control" placeholder="<?= __('Fill in to start search') ?>">
                                <span class="input-group-btn">
                <button class="btn btn-info btn-flat" type="submit"><?= __('Filter') ?></button>
                </span>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <section class="content">
                        <div class="row">
                            <!-- left column -->
                            <div class="col-md-4">
                                <!-- general form elements -->
                                <div id="supplier">
                                    <table id ="supplierListTable" class="table table-hover">
                                        <tr>
                                            <th>Item</th>
                                            <?php foreach ($stores as $store): ?>
                                                <th><?= $store->name ?></th>
                                            <?php endforeach; ?>
                                        </tr>
                                        <?php foreach ($results as $key => $result):?>
                                            <tr>
                                                <td style="width: 70%"><?= $key ?></td>
                                                <?php foreach ($result as $k=>$sstore):?>
                                                    <?php
                                                        if($k == "product_id")
                                                            continue;
                                                    ?>
                                                    <td style="width:10%;"><?= $sstore ?></td>
                                                <?php endforeach; ?>
                                            </tr>
                                        <?php endforeach; ?>

                                    </table>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <!-- general form elements -->
                                <div id="suppliers" style="width: 50%;">
                                    <select class="form-control">
                                        <option value="0">Select Supplier</option>
                                        <?php foreach ($suppliers as $supplier):?>
                                        <option value="<?= $supplier->supplier->id ?>"><?= $supplier->supplier->name ?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                                <div style="width: 100%" id="supplier_list">

                                </div>
                            </div>
                        </div>
                    </section>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>

<!-- /.content -->
<?php $this->start('scriptBotton'); ?>
<script>
    $(document).ready(function(){
        $("select").change(function(){
            var urlPath = "<?php echo \Cake\Routing\Router::Url(array('controller'=>'orders','action'=>'showSupplierOrder'))?>";
            urlPath = urlPath + "?supplier_id=" + $(this).val();
            $.ajax({
                type:'GET',
                async: true,
                cache: false,
                url:urlPath,
            }).done(function(data){
                // console.log(data);
                $("#supplier_list").html(data);
            });
        })
    });
</script>
<?php $this->end(); ?>
