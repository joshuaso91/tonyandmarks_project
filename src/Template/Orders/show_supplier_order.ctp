<table id ="supplierListTable" class="table table-hover">
    <tr>
        <th>Item</th>
        <?php foreach ($stores as $store): ?>
            <th><?= $store->name ?></th>
        <?php endforeach; ?>
    </tr>
    <?php foreach ($filters as $key => $filter):?>
        <tr>
            <td style="width: 70%"><?= $key ?></td>
            <?php foreach ($filter as $k=>$sstore):?>
                <?php
                if($k == "product_id")
                    continue;
                ?>
                <td style="width:10%;"><?= $sstore ?></td>
            <?php endforeach; ?>
        </tr>
    <?php endforeach; ?>

</table>