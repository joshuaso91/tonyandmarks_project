<!-- Content Header (Page header) -->
<style>
  td{
    cursor:pointer;
  }
</style>
<section class="content-header">
  <h1>
    Supplier's Pricing
    <div class="pull-right"><?= $this->Html->link(__('New'), ['action' => 'add'], ['class'=>'btn btn-success btn-xs']) ?></div>
  </h1>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header">
          <h3 class="box-title"><?= __('List of') ?> Supplier's Pricing / Products Suppliers</h3>
          <div class="box-tools">
            <form action="<?php echo $this->Url->build(); ?>" method="POST">
              <div class="input-group input-group-sm"  style="width: 180px;">
                <input type="text" name="search" class="form-control" placeholder="<?= __('Fill in to start search') ?>">
                <span class="input-group-btn">
                <button class="btn btn-info btn-flat" type="submit"><?= __('Filter') ?></button>
                </span>
              </div>
            </form>
          </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body table-responsive no-padding">
        <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-2">
              <!-- general form elements -->
                <div id="supplier">
                <table id ="supplierListTable" class="table table-hover">
                <?php foreach ($product_suppliers as $product_supplier): ?>
                    <tr>
                        <td id="<?php echo $product_supplier->supplier->id;?>" onclick="loadProducts(this.id);"><?= h($product_supplier->supplier->name) ?></td>
                    </tr>
                <?php endforeach; ?>
                </table>
                </div>
            </div>
              <div class="col-md-10">
                  <!-- general form elements -->
                  <div id="products">
                  
                  </div>
              </div>
        </div>
        </section>
            <?php //endforeach; ?>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          <ul class="pagination pagination-sm no-margin pull-right">
            <?php //echo $this->Paginator->numbers(); ?>
          </ul>
        </div>
      </div>
      <!-- /.box -->
    </div>
  </div>
</section>
<!-- /.content -->


<?php $this->start('scriptBotton'); ?>
<script>
    function loadProducts(supplier_id){
      var urlPath = "<?php echo \Cake\Routing\Router::Url(array('controller'=>'products-suppliers','action'=>'getSuppliersProducts'))?>";
      console.log(urlPath);
      // console.log();
      $.ajax({
          type:'GET',
          async: true,
          cache: false,
          url:urlPath+'?supplier_id='+supplier_id,
      }).done(function(data){
          // console.log(data);
          $("#products").html(data);
      });

    }
    function updateProductSupplierPrice(textInput) {
      //alert ("value" +textInput.value ); <-- check whether the value entered is received.

      console.log("value entered => " +textInput.value );
      value =   textInput.value;
      supplier_id=$(textInput).data('supplier-id');
      product_id=$(textInput).data('product-id');
      column_name=$(textInput).data('column-name');

      console.log('supplier-id: ' + supplier_id);
      console.log('product-id:' + product_id);   
      console.log('column-name: ' + column_name);

      var urlPath = "<?php echo \Cake\Routing\Router::Url(array('controller'=>'products-suppliers','action'=>'updateProductSupplierPrice'))?>";
      //alert(urlPath);

      $.ajax({
          type:'POST',
          data:{'value':value,'supplier_id':supplier_id,'product_id':product_id,'column_name':column_name},
          async: true,
          cache: false,
          url:urlPath,
          success: function(response) {
            
              console.log(response);
              var data = JSON.parse(response);
            
          },
          
          error: function(xhr,textStatus,error){
            console.log(error);
                  
          }
        
      });
      
    
    }
</script>
<?php $this->end(); ?>