<?php //debug($stores); ?>
<table class="table table-hover">
    <tr>
        <th>name</th>
        <?php foreach ($stores as $store):?>
            <th><?= $store->name ?></th>
        <?php endforeach;?>
        <th>ALL</th>
        <th>Price</th>
    </tr>
    <?php foreach ($products as $product): ?>
        <tr>
            <td style="width:20%"><?= h($product->product->name) ?> </td>
            <?php foreach ($stores as $store):?>
                <td style="width:10%"><input type="checkbox" data-store-id="<?= $store->id?>" data-product-id="<?= $product->product->id ?>"
                        <?php if($this->Check->checkOneBox($product->product->id,$store->id)){echo "checked";} ?>
                    ></td>
            <?php endforeach;?>
            <td style="width:10%"><input type="checkbox" data-store-id=0 data-product-id="<?= $product->product->id ?>"
                    <?php if($this->Check->checkAllBox($product->product->id)){echo "checked";} ?>
                ></td>
            <td style="width:40%" data-product-supplier-id="<?= $product->id ?>" data-change-price=true>
                <div class="price real">
                    <div class="price-line" data-product-supplier-id="<?= $product->id ?>">
                        <?php if(!isset($product->bargain_price)): ?>
                            <?= sprintf("$%01.2f/%s",$product->product_price,$product->product->unit); ?>
                        <?php else: ?>
                            <?= sprintf("$%01.2f/%s",$product->bargain_price,$product->product->unit); ?>
                        <?php endif; ?>

                    </div>
                    <?php if(!isset($product->bargain_price)): ?>
                        <button class="btn btn-info btn-xs btn-bgn" data-product-supplier-id="<?= $product->id ?>">Bargain</button>
                    <?php else: ?>
                        <button class="btn btn-danger btn-xs btn-bgn" data-product-supplier-id="<?= $product->id ?>">Re-Bargain</button>
                    <?php endif; ?>

                </div>
                <form method="POST" class="form-inline price new" style="display:none;" action="">
                    <div class="form-group">
                        <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
                        <div class="input-group">
                        <div class="input-group-addon">$</div>
                        <input
                                data-product-supplier-id="<?= $product->id ?>" name="price" type="text" class="form-control"
                                placeholder="was <?= isset($product->bargain_price)?sprintf("$%01.2f/%s",$product->bargain_price,$product->product->unit):sprintf("$%01.2f/%s",$product->product_price,$product->product->unit); ?>">
                        </div>
                    </div>
                </form>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
<script>
$(document).ready(function(){
    function triggleNewPrice(){
        $("td[data-change-price=true]").each(function(k,v){
            $(v).find('.new').hide();
            $(v).find('.real').show();
        });
    }
    function updateProductPrice(id,product_price){
        var urlPath = "<?php echo \Cake\Routing\Router::Url(array('controller'=>'products-suppliers','action'=>'updateProduct'))?>";
        if (product_price == "")
            return false;
        $.ajax({
            type:'POST',
            data:{
                'id':id,
                'bargain_price':product_price
            },
            async: true,
            cache: false,
            url:urlPath,
        }).done(function(data){
            data = JSON.parse(data)
            if (data.error == 0){
                $("div.price-line[data-product-supplier-id='"+id+"']").html(data.price);
            }
        });
    }
    $("input[name='price']")
    .keypress(function(e) {
        if(e.which == 13) {
            var product_id = $(this).attr('data-product-supplier-id');
            price = $(this).val();
            updateProductPrice(product_id,price);
            triggleNewPrice();
            return e.which !== 13;
        }
    })
    .on('blur',function(){
        var product_id = $(this).attr('data-product-supplier-id');
        price = $(this).val();
        updateProductPrice(product_id,price);
        triggleNewPrice();
    });
    
    $("button.btn-bgn").on('click',function(){
        triggleNewPrice();
        p_id = $(this).attr('data-product-supplier-id');
        $('td[data-product-supplier-id="'+p_id+'"]').find('.real').hide();
        $('td[data-product-supplier-id="'+p_id+'"]').find('.new').show();
        $('td[data-product-supplier-id="'+p_id+'"]').find('.new').find("div").find('input').focus();
    });


    $("input[type='checkbox']").change(function(){
        product_id = $(this).attr('data-product-id');
        store_id = $(this).attr('data-store-id');
        action = "add";
        if ($(this).prop('checked') == false)
            action = "delete";
        var urlPath = "<?php echo \Cake\Routing\Router::Url(array('controller'=>'products-suppliers','action'=>'updateOrder'))?>";
        $.ajax({
            type:'POST',
            data:{
                'id':product_id,
                'store_id':store_id,
                'action':action
            },
            async: true,
            cache: false,
            url:urlPath,
        }).done(function(data){
            data = JSON.parse(data);
//            if (data.error == 0){
//                $("div.price-line[data-product-supplier-id='"+id+"']").html(data.price);
//            }
        });
    });
});
</script>