<section class="content-header">
  <h1>
    Supplier
    <small><?= __('Add') ?></small>
  </h1>
  <ol class="breadcrumb">
    <li>
    <?= $this->Html->link('<i class="fa fa-dashboard"></i> '.__('Back'), ['action' => 'index'], ['escape' => false]) ?>
    </li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-6">
      <!-- general form elements -->
        <div id="supplier">

        </div>
    </div>

      <div class="col-md-6">
          <!-- general form elements -->
          <div id="products">

          </div>
      </div>
  </div>
</section>
<?php $this->start('scriptBotton'); ?>
<script>
    $(document).ready(function () {

        $("#supplier").load('/suppliers/supplierList');

        $('#supplierListTable tr td').click(function(){
            var cid = $(this).attr('id');
            alert(cid);
            var cval = $(this).text();
            alert(cval);
        });

    });
    function move(td) {
        console.log(td.id);
    }

    function loadProducts(td)
    {
        $("#products").load('/ProductsSuppliers/supplierProducts/'+td.id);
    }

    function doalert(checkboxElem) {
        if (checkboxElem.checked) {
            alert ($(checkboxElem).data('supplier-id'));
            $(checkboxElem).data('supplier-id');

        } else {
            alert ("bye");
        }
    }

</script>
<?php $this->end(); ?>


<checkbox data-supllier-id="2" data-store-id="1" data-product-id="2"></checkbox>
