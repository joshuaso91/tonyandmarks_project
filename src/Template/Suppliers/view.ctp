<section class="content-header">
  <h1>
    <?php echo __('Supplier'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false])?>
    </li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-info"></i>
                <h3 class="box-title"><?php echo __('Information'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                                                                                                                <dt><?= __('Name') ?></dt>
                                        <dd>
                                            <?= h($supplier->name) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Contact Name') ?></dt>
                                        <dd>
                                            <?= h($supplier->contact_name) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Email') ?></dt>
                                        <dd>
                                            <?= h($supplier->email) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Contact Phone') ?></dt>
                                        <dd>
                                            <?= h($supplier->contact_phone) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Phone No') ?></dt>
                                        <dd>
                                            <?= h($supplier->phone_no) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Address') ?></dt>
                                        <dd>
                                            <?= h($supplier->address) ?>
                                        </dd>
                                                                                                                                                            <dt><?= __('Contact Notes') ?></dt>
                                        <dd>
                                            <?= h($supplier->contact_notes) ?>
                                        </dd>
                                                                                                                                    
                                            
                                                                                                                                            
                                                                                                                                                                                                
                                            
                                    </dl>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Same Day Orders']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($supplier->same_day_orders)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Store Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    User Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Supplier Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Product Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Ordered Qty
                                    </th>
                                        
                                                                    
                                    <th>
                                    Order Date
                                    </th>
                                        
                                                                    
                                    <th>
                                    Same Day Order
                                    </th>
                                        
                                                                    
                                    <th>
                                    Order Notes
                                    </th>
                                        
                                                                                                                                            
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($supplier->same_day_orders as $sameDayOrders): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($sameDayOrders->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($sameDayOrders->store_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($sameDayOrders->user_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($sameDayOrders->supplier_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($sameDayOrders->product_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($sameDayOrders->ordered_qty) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($sameDayOrders->order_date) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($sameDayOrders->same_day_order) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($sameDayOrders->order_notes) ?>
                                    </td>
                                                                                                            
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'SameDayOrders', 'action' => 'view', $sameDayOrders->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'SameDayOrders', 'action' => 'edit', $sameDayOrders->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SameDayOrders', 'action' => 'delete', $sameDayOrders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sameDayOrders->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Products']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($supplier->products)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Unit
                                    </th>
                                        
                                                                    
                                    <th>
                                    Notes
                                    </th>
                                        
                                                                                                                                            
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($supplier->products as $products): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($products->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($products->name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($products->unit) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($products->notes) ?>
                                    </td>
                                                                                                            
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Products', 'action' => 'view', $products->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Products', 'action' => 'edit', $products->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Products', 'action' => 'delete', $products->id], ['confirm' => __('Are you sure you want to delete # {0}?', $products->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Stores']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($supplier->stores)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Email
                                    </th>
                                        
                                                                    
                                    <th>
                                    Phone No
                                    </th>
                                        
                                                                    
                                    <th>
                                    Address
                                    </th>
                                        
                                                                                                                                            
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($supplier->stores as $stores): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($stores->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($stores->name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($stores->email) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($stores->phone_no) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($stores->address) ?>
                                    </td>
                                                                                                            
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Stores', 'action' => 'view', $stores->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Stores', 'action' => 'edit', $stores->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Stores', 'action' => 'delete', $stores->id], ['confirm' => __('Are you sure you want to delete # {0}?', $stores->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
