<table id ="supplierListTable" class="table table-hover">
    <tr>

        <th><?= $this->Paginator->sort('name') ?></th>

    </tr>
<?php foreach ($suppliers as $supplier): ?>

    <tr>

        <td id="<?php echo $supplier->id;?>" onclick="loadProducts(this);"><?= h($supplier->name) ?></td>
    </tr>
<?php endforeach; ?>
</table>
