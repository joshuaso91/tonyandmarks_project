<section class="content-header">
  <h1>
    <?php echo __('Product'); ?>
  </h1>
  <ol class="breadcrumb">
    <li>
    <?= $this->Html->link('<i class="fa fa-dashboard"></i> ' . __('Back'), ['action' => 'index'], ['escape' => false])?>
    </li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <i class="fa fa-info"></i>
                <h3 class="box-title"><?php echo __('Information'); ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <dl class="dl-horizontal">
                                         <dt><?= __('Name') ?></dt>
                                        <dd>
                                            <?= h($product->name) ?>
                                        </dd>
                                                                                                                                    
                                            
                                                                                                                                            
                                                                                                                                                                                                
                                            
                                                                        <dt><?= __('Notes') ?></dt>
                            <dd>
                            <?= $this->Text->autoParagraph(h($product->notes)); ?>
                            </dd>
                                                            </dl>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- ./col -->
</div>
<!-- div -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Same Day Orders']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($product->same_day_orders)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Store Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    User Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Supplier Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Product Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Ordered Qty
                                    </th>
                                        
                                                                    
                                    <th>
                                    Order Date
                                    </th>
                                        
                                                                    
                                    <th>
                                    Same Day Order
                                    </th>
                                        
                                                                    
                                    <th>
                                    Order Notes
                                    </th>
                                        
                                                                                                                                            
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($product->same_day_orders as $sameDayOrders): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($sameDayOrders->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($sameDayOrders->store_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($sameDayOrders->user_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($sameDayOrders->supplier_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($sameDayOrders->product_id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($sameDayOrders->ordered_qty) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($sameDayOrders->order_date) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($sameDayOrders->same_day_order) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($sameDayOrders->order_notes) ?>
                                    </td>
                                                                                                            
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'SameDayOrders', 'action' => 'view', $sameDayOrders->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'SameDayOrders', 'action' => 'edit', $sameDayOrders->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'SameDayOrders', 'action' => 'delete', $sameDayOrders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $sameDayOrders->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Stores']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($product->stores)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Email
                                    </th>
                                        
                                                                    
                                    <th>
                                    Phone No
                                    </th>
                                        
                                                                    
                                    <th>
                                    Address
                                    </th>
                                        
                                                                                                                                            
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($product->stores as $stores): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($stores->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($stores->name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($stores->email) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($stores->phone_no) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($stores->address) ?>
                                    </td>
                                                                                                            
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Stores', 'action' => 'view', $stores->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Stores', 'action' => 'edit', $stores->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Stores', 'action' => 'delete', $stores->id], ['confirm' => __('Are you sure you want to delete # {0}?', $stores->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Related {0}', ['Suppliers']) ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                <?php if (!empty($product->suppliers)): ?>

                    <table class="table table-hover">
                        <tbody>
                            <tr>
                                                                    
                                    <th>
                                    Id
                                    </th>
                                        
                                                                    
                                    <th>
                                    Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Contact Name
                                    </th>
                                        
                                                                    
                                    <th>
                                    Email
                                    </th>
                                        
                                                                    
                                    <th>
                                    Contact Phone
                                    </th>
                                        
                                                                    
                                    <th>
                                    Phone No
                                    </th>
                                        
                                                                    
                                    <th>
                                    Address
                                    </th>
                                        
                                                                    
                                    <th>
                                    Contact Notes
                                    </th>
                                        
                                                                                                                                            
                                <th>
                                    <?php echo __('Actions'); ?>
                                </th>
                            </tr>

                            <?php foreach ($product->suppliers as $suppliers): ?>
                                <tr>
                                                                        
                                    <td>
                                    <?= h($suppliers->id) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($suppliers->name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($suppliers->contact_name) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($suppliers->email) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($suppliers->contact_phone) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($suppliers->phone_no) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($suppliers->address) ?>
                                    </td>
                                                                        
                                    <td>
                                    <?= h($suppliers->contact_notes) ?>
                                    </td>
                                                                                                            
                                                                        <td class="actions">
                                    <?= $this->Html->link(__('View'), ['controller' => 'Suppliers', 'action' => 'view', $suppliers->id], ['class'=>'btn btn-info btn-xs']) ?>

                                    <?= $this->Html->link(__('Edit'), ['controller' => 'Suppliers', 'action' => 'edit', $suppliers->id], ['class'=>'btn btn-warning btn-xs']) ?>

                                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Suppliers', 'action' => 'delete', $suppliers->id], ['confirm' => __('Are you sure you want to delete # {0}?', $suppliers->id), 'class'=>'btn btn-danger btn-xs']) ?>    
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                                    
                        </tbody>
                    </table>

                <?php endif; ?>

                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
