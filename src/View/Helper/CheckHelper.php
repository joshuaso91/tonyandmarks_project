<?php
/* src/View/Helper/LinkHelper.php */
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\ORM\TableRegistry;

class CheckHelper extends Helper
{
    public function checkOneBox($p_id, $s_id)
    {
        $ps = TableRegistry::get('ProductsStores');
        $res = $ps->find('all')->where([
            'store_id'=>$s_id,
            'product_id'=>$p_id
        ])->toArray();
        if (count($res) == 0) {
            return false;
        }
        return true;
    }


    public function checkAllBox($p_id)
    {
        $stores = TableRegistry::get('stores')->find('all')->toArray();
        $c = 0;
        foreach ($stores as $store) {
            $res = TableRegistry::get('ProductsStores')->find('all')->where([
                'store_id'=>$store->id,
                'product_id'=>$p_id
            ])->toArray();
            if (count($res)!=0) {
                $c += 1;
            }
        }
        if ($c == count($stores)) {
            return true;
        }
        return false;
    }
}
